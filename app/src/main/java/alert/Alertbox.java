package alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;




public class Alertbox {
	private Context context;
	public AlertDialog alertDialog;
	public Alertbox(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	
	public void showAlertbox(String msg)
	{
		alertDialog = new AlertDialog.Builder(context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.alertbox, null);
		alertDialog.setView(dialogView);

		TextView log = (TextView) dialogView.findViewById(R.id.textView1);
		Button okay = (Button)  dialogView.findViewById(R.id.okay);
		log.setText(msg);
		okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
		alertDialog.show();
	}



}
