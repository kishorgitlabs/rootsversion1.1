package apiservice;


import model.UploadDoc.Doc;
import model.appointment.AppointmentResult;
import model.productscontacts.ProductsContacts;
import model.register.RegisterResult;
import model.videocategory.VideoCategory;
import model.videos.VideosResult;
import model.whatsnew.WhatsNewResult;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Systems02 on 10-May-17.
 */

public interface APIService {

    //Registration
    @FormUrlEncoded
    @POST("api/Value/Register")
    Call<RegisterResult> REGISTER_RESULT_CALL(
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("city") String city,
            @Field("state") String state,
            @Field("appnotificationid") String appnotificationid,
            @Field("customer_type") String customer_type
    );

    //ProductsDetails
    @GET("api/Value/GetProductDetails")
    Call<ProductsContacts> PRODUCTS_CONTACTS_CALL();

    //Appointment
    @FormUrlEncoded
    @POST("api/Value/Appointment")
    Call<AppointmentResult> APPOINTMENT_RESULT_CALL(
            @Field("Name") String Name,
            @Field("MobileNo") String MobileNo,
            @Field("EmailId") String EmailId,
            @Field("Address") String Address,
            @Field("ProductName") String ProductName,
            @Field("AppointmentDate") String AppointmentDate,
            @Field("AppointmentTime") String AppointmentTime,
            @Field("Description") String Description,
            @Field("City") String city,
            @Field("CompanyName") String companyname
    );

    //Whats New
    @GET("api/Value/WhatsNew")
    Call<WhatsNewResult> WHATS_NEW_RESULT_CALL();


    //GetVideosCategory
    @GET("api/Value/GetVideoCategory")
    Call<VideoCategory> VIDEO_CATEGORY_CALL();

    //Get Videos
    @FormUrlEncoded
    @POST("api/Value/GetVideoByCategorywise")
    Call<VideosResult> VIDEOS_RESULT_CALL(
            @Field("VideoCatagoryname") String VideoCatagoryname

    );

    //Get Doc
    @GET("api/value/uploaddoc")
    Call<Doc> GetDoc();


}
