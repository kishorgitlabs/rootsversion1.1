package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

;import model.videos.Datum;


/**
 * Created by Systems02 on 12/10/2016.
 */

public class Videos_Adapter extends ArrayAdapter<Datum> {


    List<Datum> Datumists;
    final Pattern p = Pattern.compile("^\\d+");
    final static String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    Context context;
    public Videos_Adapter(Context context, List<Datum> Datumists)
    {
        super(context, 0, Datumists);
        this.Datumists=Datumists;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_videos, parent, false);
            TextView Datumtext=(TextView)convertView.findViewById(R.id.list_item);
            ImageView Image = (ImageView)convertView.findViewById(R.id.video_image);

            Datumtext.setText(Datumists.get(position).getVideoname());
            String thumbnail = getVideoId(Datumists.get(position).getVideoURL());

            if(Datumists.get(position).getVideoURL().contains("="))
            {
                Picasso.with(context)
                        .load("http://img.youtube.com/vi/"+thumbnail+"/0.jpg")
                        .fit()
                        .error(R.drawable.no_image)
                        .into(Image);
            }
            else
            {
                Picasso.with(context)
                        .load("http://img.youtube.com/vi/"+thumbnail+"/0.jpg")
                        .fit()
                        .error(R.drawable.no_image)
                        .into( Image);
            }


            /*   try {
                // get input stream
                InputStream ims = context.getAssets().open(Datumists.get(position)+".png");
                // load image as Drawable
                Drawable d = Drawable.createFromStream(ims, null);
                // set image to ImageView
                Image.setImageDrawable(d);
            }
            catch(IOException ignored) {

            }

*/

        }
        return convertView;
    }

    public static String getVideoId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return null;

        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return null;
    }

}