package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.whatsnew.Datum;


/**
 * Created by Systems02 on 12/10/2016.
 */

public class Whatnew_Adapter extends ArrayAdapter<Datum> {


    private List<Datum> Datumists;

    private Context context;

    public Whatnew_Adapter(Context context, List<Datum> Datumists) {
        super(context, 0, Datumists);
        this.Datumists = Datumists;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.whatsnew_adapter, parent, false);
            TextView productname = (TextView) convertView.findViewById(R.id.productname);
            TextView category = (TextView) convertView.findViewById(R.id.category);
           // TextView sub_category = (TextView) convertView.findViewById(R.id.sub_category);
            ImageView product_image = (ImageView) convertView.findViewById(R.id.product_image);


            productname.setText(Datumists.get(position).getProductName());
            category.setText(String.format("%s",Datumists.get(position).getProductCategory()));
           // sub_category.setText(String.format("%s",Datumists.get(position).getProductSubCategory()));

            try {
                /*Glide.with(context)
                        .load(Datumists.get(position).getProductURL())
                        .into(product_image);*/
                Picasso.with(context).load(Datumists.get(position).getProductURL()).fit().error(R.drawable.no_image).into(product_image);
            } catch (Exception ignored) {
                Picasso.with(context).load(R.drawable.no_image).into(product_image);
                Log.v("Image Exception", ignored.getMessage());
            }


        }
        return convertView;
    }

}