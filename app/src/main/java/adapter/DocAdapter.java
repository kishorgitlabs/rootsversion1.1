package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;

import java.util.List;

import model.UploadDoc.DocList;
import retroclient.RetroClient;

public class DocAdapter extends RecyclerView.Adapter<DocAdapter.ViewHolder> {

    List<DocList> list;
    Context context;

    public DocAdapter(List<DocList> list,Context context){
        this.list = list;
        this.context = context;
    }
    @NonNull
    @Override
    public DocAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.doc_adapter, parent, false);
        return new DocAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DocAdapter.ViewHolder holder, int position) {

        holder.txt.setText(list.get(position).getTitle());
        holder.txt1.setText(list.get(position).getFilename());
        holder.s_no.setText(list.get(position).getId());
        holder.txt1.setVisibility(View.GONE);

//        String s = context.getString(Integer.parseInt(list.get(position).getFilename().toString()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt,txt1,s_no;
        ImageView imageView;
        String string;

        public ViewHolder(View itemView) {
            super(itemView);
            txt = itemView.findViewById(R.id.txt1);
            txt1 = itemView.findViewById(R.id.txt2);
            s_no = itemView.findViewById(R.id.s_no);
            imageView = itemView.findViewById(R.id.downdoc);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String a = RetroClient.getString();
                    String string = a + "/" + "ImageUpload" + "/" + txt1.getText().toString();
                    Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(string));
                    if(searchAddress!=null) {
                        v.getContext().startActivity(searchAddress);
                    }
                }
            });
        }
    }
}
