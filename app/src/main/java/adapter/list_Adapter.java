package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import model.productscontacts.Product;


/**
 * Created by Systems02 on 12/10/2016.
 */

public class list_Adapter extends ArrayAdapter<Product> {


    List<Product> productists;

    Context context;
    public list_Adapter(Context context, List<Product> productists)
    {
        super(context, 0, productists);
        this.productists=productists;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_adapter, parent, false);
            TextView producttext= (TextView)convertView.findViewById(R.id.list_item);
            ImageView Image = (ImageView)convertView.findViewById(R.id.product_image);



                    producttext.setText(productists.get(position).getProductName());

                    try {
                        Picasso.with(context)
                                .load(productists.get(position).getProductURL()).error(R.drawable.no_image)
                                .into(Image)
                        ;
                        //Log.v("Image URL", product.getProductURL());
                    } catch (Exception e) {
                        Log.v("Error_Prod_Image", e.getMessage());
                        //Picasso.with(context).load(acctistList.get(position).getProductURL()).fit().error(R.drawable.noimage).into(image);
                    }











        }
        return convertView;
    }

}