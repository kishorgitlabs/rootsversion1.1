package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roots.brainmagic.rootsindia.R;
import com.roots.brainmagic.rootsindia.Videos_Activity;

import java.util.List;

import model.videocategory.Datum;

import static indent.Intent_Constants.Intent_VideoCategory;


/**
 * Created by Systems02 on 12/10/2016.
 */

public class VideoCategory_Adapter extends ArrayAdapter<Datum> {


    private List<Datum> Datumists;

    private Context context;

    public VideoCategory_Adapter(Context context, List<Datum> Datumists) {
        super(context, 0, Datumists);
        this.Datumists = Datumists;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_videocategory, parent, false);
            TextView productname = (TextView) convertView.findViewById(R.id.listitem);
            productname.setText(Datumists.get(position).getVideoCatagoryname());
            productname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Videos_Activity.class).putExtra(Intent_VideoCategory,Datumists.get(position).getVideoCatagoryname()));
                }
            });
        }
        return convertView;
    }

}