package com.roots.brainmagic.rootsindia;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapter.list_Adapter;
import alert.Alertbox;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.productscontacts.Product;
import presistance.sqlitedatabase.AppDatabase;
import presistance.transaction.Transaction;
import toaster.Toasts;

import static indent.Intent_Constants.Intent_Category;
import static indent.Intent_Constants.Intent_ProductID;
import static indent.Intent_Constants.Intent_ProductName;
import static indent.Intent_Constants.Intent_Search;
import static indent.Intent_Constants.Intent_SubCAtegory;
import static presistance.sqlitedatabase.AppDatabase.destroyInstance;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;
import static presistance.transaction.Transaction.getProductlist;

public class Search_ProductList_Activity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    LinearLayout Headerlayout;
    @BindView(R.id.productlistview)
    ListView Productlistview;
    @BindView(R.id.navi)
    TextView navi;

    List<Product> Productlist;
    AppDatabase db;
    private String SearchString,SearchText;
    Alertbox alert = new Alertbox(Search_ProductList_Activity.this);
    Toasts toasts = new Toasts(Search_ProductList_Activity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product_list);
        ButterKnife.bind(this);
        LinearLayout Menulayout  = Headerlayout.findViewById(R.id.menu);
        Menulayout.setVisibility(View.INVISIBLE);
        //Productlistview = new
        db = getAppDatabase(Search_ProductList_Activity.this);
        SearchString = getIntent().getStringExtra(Intent_Search);
        SearchText = "%"+SearchString+"%";
        navi.setText(String.format("%s", "Search Result for \""+SearchString+"\""));
        Productlist = new ArrayList<>();
        Get_Produclist();



        Productlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(Search_ProductList_Activity.this,
                        ProductDetails_Activity.class)
                        .putExtra(Intent_Category,"")
                        .putExtra(Intent_SubCAtegory,"")
                        .putExtra(Intent_ProductName,Productlist.get(position).getProductName())
                        .putExtra(Intent_ProductID,Productlist.get(position).getId())
                );
            }
        });

    }




    private void Get_Produclist() {

        Productlist = Transaction.getSearchResult(db,SearchText);
        if(Productlist.size() == 0) {
            alert.showAlertbox("No Products found for this search");
            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }
        else{
            Productlistview.setAdapter(new list_Adapter(Search_ProductList_Activity.this, Productlist));
        }

    }
    public void Menu(View view) {
    }

    @Override
    protected void onDestroy() {
        destroyInstance();
        super.onDestroy();
    }
}
