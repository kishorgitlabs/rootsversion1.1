package com.roots.brainmagic.rootsindia;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import static shared.Shared.K_Register;
import static shared.Shared.MyPREFERENCES;

public class Splash_Activity extends Activity {
    private static final long SPLASH_DISPLAY_LENGTH = 1500;
    Boolean isregister;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);


        myshare = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        isregister = myshare.getBoolean(K_Register, false);
        editor.apply();
        //editor.putInt("VERSION",15).apply();

		 /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/

        Handler();

    }


    void Handler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isregister) {
                    startActivity(new Intent(Splash_Activity.this, Home_Activity.class));
                    finish();
                } else {
                    startActivity(new Intent(Splash_Activity.this, Register_Activity.class));
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);


    }
}