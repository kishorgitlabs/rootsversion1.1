package com.roots.brainmagic.rootsindia;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;

import model.productscontacts.Contact;
import model.productscontacts.Product;
import model.productscontacts.ProductsContacts;
import network.NetworkConnection;
import presistance.sqlitedatabase.AppDatabase;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared.Shared;

import static presistance.sqlitedatabase.AppDatabase.destroyInstance;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;
import static presistance.transaction.Transaction.addContacts;
import static presistance.transaction.Transaction.addProducts;
import static presistance.transaction.Transaction.deleteContacts;
import static presistance.transaction.Transaction.deletePrducts;
import static shared.Shared.K_Update;

public class Home_Activity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    LinearLayout Headerlayout;
//    @BindView(R.id.appointment_image)
//    CircularImageView circularImageView_app;
//    @BindView(R.id.update_image)
//    CircularImageView circularImageView_update;

//
//    @BindView(R.id.productcard)
//    CardView productcard;

//    @BindView(R.id.whatsnew)
//    CardView whatsnew;
//
//    @BindView(R.id.videoss)
//    CardView videoss;
//
//    @BindView(R.id.appoinment)
//    CardView appoinment;
//
//    @BindView(R.id.checkupdate)
//    CardView checkupdate;

    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(Home_Activity.this);
    Alertbox alert = new Alertbox(Home_Activity.this);
    AppDatabase db;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private List<Product> productist;
    private int count = 0;
    private int old = 0;
    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        productist = new ArrayList<>();
        animation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        //GlideApp.
        //Glide.circularImageView_app.setImageDrawable(R.drawable.appointment);

//        Picasso.with(this).load("file:///android_asset/app.jpg").error(R.drawable.no_image).into(circularImageView_app);
//        Picasso.with(this).load("file:///android_asset/pupdate.jpg").error(R.drawable.no_image).into(circularImageView_update);
        /*Picasso.with(this)
                .load(productists.get(position).getProductURL()).error(R.drawable.no_image)
                .into(Image);
        Glide.with(this).load(getImage("app.jpg")).into(circularImageView_app);*/
        /*Glide.with(myFragment)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.loading_spinner)
                .into(myImageView);*/
        LinearLayout Menulayout = Headerlayout.findViewById(R.id.menu);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();
        db = getAppDatabase(Home_Activity.this);

        if (!myshare.getBoolean(K_Update, false))
            StartUpdates();

//        productcard.setAnimation(animation);
//        whatsnew.setAnimation(animation);
//        appoinment.setAnimation(animation);
//        videoss.setAnimation(animation);
//        checkupdate.setAnimation(animation);
    }

    public int getImage(String imageName) {
        return this.getResources().getIdentifier(imageName, "drawable", this.getPackageName());
    }

    public void StartUpdates() {
        if (network.CheckInternet())
            Update_SQLiteDB();
        else
            alert.showAlertbox(getString(R.string.NoNetowrk));
    }

    public void Product_Catalogue(View view) {
        startActivity(new Intent(Home_Activity.this, ProductActivity.class));
    }

    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(Home_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(Home_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(Home_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(Home_Activity.this, WhatsNew_Activity.class);
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(Home_Activity.this, Search_Products_Activity.class);
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(Home_Activity.this, VideoCategory_Activity.class);
                        startActivity(enquiry);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(Home_Activity.this, Contact_Activity.class);
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(Home_Activity.this, Appointment_Activity.class);
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
        popupMenu.getMenu().findItem(R.id.home).setVisible(false);
        popupMenu.show();

    }

    @Override
    protected void onDestroy() {
        destroyInstance();
        super.onDestroy();
    }

    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast,
                    (ViewGroup) findViewById(R.id.custom_toast_container));

            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Press " + "'Back'" + " once again to exit!");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL | Gravity.FILL_HORIZONTAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();


        }
        back_pressed = System.currentTimeMillis();
    }


    private void Update_SQLiteDB() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Downloading data from online database please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            editor.putInt("olddatacount", old);
            editor.commit();
            editor.apply();
            deletePrducts(db);
            deleteContacts(db);
            Call<ProductsContacts> call = service.PRODUCTS_CONTACTS_CALL();
            call.enqueue(new Callback<ProductsContacts>() {
                @Override
                public void onResponse(Call<ProductsContacts> call, Response<ProductsContacts> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {
                        case "Success": {
                            if (response.body().getProduct() != null) {
                                productist = response.body().getProduct();
                                old = productist.size();
                                count = productist.size() - myshare.getInt("olddatacount", 0);
                                addProducts(db, productist);
                            }
                            if (response.body().getContact() != null) {
                                List<Contact> contacts = response.body().getContact();
                                addContacts(db, contacts);

                                editor.putBoolean(K_Update, true);
                                editor.apply();
                                progressDialog.dismiss();
                                alert.showAlertbox("Products updated Successfully");
                            }
                            else {

//                                productist = response.body().getProduct();
//                                old = productist.size();
//                                count = productist.size() - myshare.getInt("olddatacount", 0);
//                                addProducts(db, productist);
                                progressDialog.dismiss();
                                showAlertbox(getString(R.string.download_failed));
                            }

                            break;
                        }
                        default: {
                            progressDialog.dismiss();
                            editor.putBoolean(K_Update, false);
                            editor.apply();
                            showAlertbox(getString(R.string.download_failed));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductsContacts> call, Throwable t) {
                    progressDialog.dismiss();
                    editor.putBoolean(K_Update, false);
                    editor.apply();
                    showAlertbox(getString(R.string.download_failed));

                }
            });

        } catch (Exception ex) {
            Log.v("Download Exception", ex.getMessage());
            editor.apply();
            showAlertbox(getString(R.string.download_failed));
        }
    }

    public void showAlertbox(String msg) {
        final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();

        LayoutInflater inflater = ((Activity) Home_Activity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdownload, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        Button cancel = (Button) dialogView.findViewById(R.id.cancel);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                StartUpdates();
                alertDialog.dismiss();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void Contact_Us(View view) {
        startActivity(new Intent(Home_Activity.this, Contact_Activity.class));
    }

    public void Search_Products(View view) {
        startActivity(new Intent(Home_Activity.this, Search_Products_Activity.class));
    }

    public void Appointment(View view) {
        startActivity(new Intent(Home_Activity.this, Appointment_Activity.class));
    }

    public void Whats_New(View view) {
        startActivity(new Intent(Home_Activity.this, WhatsNew_Activity.class));
    }


    public void Check_Update(View view) {
        StartUpdates();
    }

    public void Videos(View view) {

        startActivity(new Intent(Home_Activity.this, VideoCategory_Activity.class));
    }

    public void Catalogue(View view) {
        startActivity(new Intent(Home_Activity.this, CatalogueActivity.class));
    }
}
