package com.roots.brainmagic.rootsindia;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import adapter.ExpandableListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import presistance.sqlitedatabase.AppDatabase;
import toaster.Toasts;

import static indent.Intent_Constants.Intent_Category;
import static indent.Intent_Constants.Intent_Search;
import static indent.Intent_Constants.Intent_SubCAtegory;
import static presistance.sqlitedatabase.AppDatabase.destroyInstance;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;
import static presistance.transaction.Transaction.getMainCategory;
import static presistance.transaction.Transaction.getProductlist;


public class ProductActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    LinearLayout Headerlayout;
    @BindView(R.id.expandable_view)
    ExpandableListView expandableListView;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ExpandableListAdapter listAdapter;
    private ExpandableListView ExpandList;
    TextView mProduct1;
    AppDatabase db;
    @BindView(R.id.search_text)
    EditText SearchText;
    Toasts toasts = new Toasts(ProductActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        ButterKnife.bind(this);
        db = getAppDatabase(ProductActivity.this);
        LinearLayout Menulayout = Headerlayout.findViewById(R.id.menu);
        //  mProduct1 = (TextView) findViewById(R.id.product1);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
      //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)​;
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expandableListView.setAdapter(listAdapter);
       // expandableListView.setIndicatorBounds(expandableListView.getRight(), expandableListView.getWidth());
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                startActivity(new Intent(ProductActivity.this,Product_List_Activity.class)
                        .putExtra(Intent_Category,listDataHeader.get(groupPosition))
                        .putExtra(Intent_SubCAtegory,listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition)));
                return false;
            }
        });


    }
    /*public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }*/

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding Header data
        listDataHeader = getMainCategory(db);

        // Adding child data
        for (String Header : listDataHeader) {
            listDataChild.put(Header, getProductlist(db, Header));
        }
        }

    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(ProductActivity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(ProductActivity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(ProductActivity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(ProductActivity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(ProductActivity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(ProductActivity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(ProductActivity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(ProductActivity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(ProductActivity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.products).setVisible(false);
        popupMenu.show();

    }

    @Override
    protected void onDestroy() {
        destroyInstance();
        super.onDestroy();
    }


    public void Search_Products(View view) {
        String Search_String = SearchText.getText().toString();
        if(Search_String.length() == 0)
            toasts.ShowErrorToast("Search field cannot be empty");
        else
            startActivity(new Intent(ProductActivity.this,Search_ProductList_Activity.class).putExtra(Intent_Search,Search_String));
    }


}
/*  *//* listDataHeader.add("Sweepers");
        listDataHeader.add("Scrubbers");
        listDataHeader.add("Escalator Cleaner");
        listDataHeader.add("Vacuum Cleaners");
        listDataHeader.add("High Pressure Jets");
        listDataHeader.add("Carpet Care");
        listDataHeader.add("Steam Cleaners");
        listDataHeader.add("Janitorial");
        listDataHeader.add("Detergents and Pads");*//*

        // Adding child data

        List<String> Sweepers = new ArrayList<String>();
        Sweepers.add("Walk Behind");
        Sweepers.add("Ride On");
        Sweepers.add("Truck Mounted Sweeper");
        Sweepers.add("City Sweeper");

        List<String> Scrubbers = new ArrayList<String>();
        Scrubbers.add("Walk Behind");
        Scrubbers.add("Ride On");
        List<String> Escalator = new ArrayList<String>();
        Escalator.add("Escalator/Travelator Cleaner");
        List<String> Vacuum = new ArrayList<String>();
        Vacuum.add("Domestic");
        Vacuum.add("Commercial");
        Vacuum.add("Industrial");
        List<String> High = new ArrayList<String>();
        List<String> Carpet = new ArrayList<String>();
        List<String> Steam = new ArrayList<String>();
        List<String> Janitorial = new ArrayList<String>();
        List<String> Detergents = new ArrayList<String>();


        // Adding child data
*/