package com.roots.brainmagic.rootsindia;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

public class Contact_Activity extends AppCompatActivity {
    private TextView weblink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        weblink=(TextView)findViewById(R.id.weblink);
       /* weblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(("www.rootsmulticlean.com")));
                startActivity(searchAddress);

            }
        });*/
    }
    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(Contact_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(Contact_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(Contact_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(Contact_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(Contact_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(Contact_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(Contact_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(Contact_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(Contact_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
        popupMenu.getMenu().findItem(R.id.contact).setVisible(false);
        popupMenu.show();

    }
}
