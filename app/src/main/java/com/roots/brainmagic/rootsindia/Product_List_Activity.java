package com.roots.brainmagic.rootsindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapter.list_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.productscontacts.Product;
import presistance.sqlitedatabase.AppDatabase;
import presistance.transaction.Transaction;

import static indent.Intent_Constants.Intent_Category;
import static indent.Intent_Constants.Intent_ProductID;
import static indent.Intent_Constants.Intent_ProductName;
import static indent.Intent_Constants.Intent_SubCAtegory;
import static presistance.sqlitedatabase.AppDatabase.destroyInstance;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;
import static presistance.transaction.Transaction.getProductlist;

public class Product_List_Activity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    LinearLayout Headerlayout;
    @BindView(R.id.productlistview)
    ListView Productlistview;
    @BindView(R.id.navi)
    TextView navi;

    String Category, SubCategory;
    int ProductID;
    List<Product> Productlist;
    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        ButterKnife.bind(this);
        LinearLayout Menulayout = Headerlayout.findViewById(R.id.menu);
        Menulayout.setVisibility(View.INVISIBLE);
        //Productlistview = new
        db = getAppDatabase(Product_List_Activity.this);
        Category = getIntent().getStringExtra(Intent_Category);
        SubCategory = getIntent().getStringExtra(Intent_SubCAtegory);
        ProductID = getIntent().getIntExtra(Intent_ProductID, 0);

        navi.setText(String.format("%s >>", Category));
        Productlist = new ArrayList<>();
        Get_Produclist();


       /* ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.ListView,Productlist);
        Productlistview.setAdapter(adapter);*/
        /*RelativeLayout sub_category1 = (RelativeLayout) findViewById(R.id.sub_category1);
        sub_category1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Product_List_Activity.this,
                        ProductDetails_Activity.class).putExtra("navi",navi.getText().toString()+" >> RootsScrub RB 800 "));
            }
        });*/
        Productlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(Product_List_Activity.this,
                        ProductDetails_Activity.class)
                        .putExtra(Intent_Category, Category)
                        .putExtra(Intent_SubCAtegory, SubCategory)
                        .putExtra(Intent_ProductName, Productlist.get(position).getProductName())
                        .putExtra(Intent_ProductID, Productlist.get(position).getId())
                );
            }
        });

    }


    private void Get_Produclist() {

        try {
            Productlist = Transaction.getProductlist(db, Category, SubCategory);
//

            Productlistview.setAdapter(new list_Adapter(Product_List_Activity.this, Productlist));
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void Menu(View view) {
    }

    @Override
    protected void onDestroy() {
        destroyInstance();
        super.onDestroy();
    }
}
