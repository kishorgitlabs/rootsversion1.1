package com.roots.brainmagic.rootsindia;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import adapter.Whatnew_Adapter;

import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;

import model.productscontacts.Product;
import model.whatsnew.Datum;
import model.whatsnew.WhatsNewResult;
import presistance.sqlitedatabase.AppDatabase;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static indent.Intent_Constants.Intent_Category;
import static indent.Intent_Constants.Intent_ProductID;
import static indent.Intent_Constants.Intent_ProductName;
import static indent.Intent_Constants.Intent_SubCAtegory;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;


public class WhatsNew_Activity extends AppCompatActivity {
    @BindView(R.id.listview)
    ListView listView;
    private List<Datum> Whatnew_list;
    Alertbox alert = new Alertbox(this);
    AppDatabase db;
    String Category,SubCategory;
    int ProductID;
  //  List<Product> Productlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);
        ButterKnife.bind(this);
        db = getAppDatabase(WhatsNew_Activity.this);
      //  Productlist = new ArrayList<>();
        Category = getIntent().getStringExtra(Intent_Category);
        SubCategory = getIntent().getStringExtra(Intent_SubCAtegory);
        ProductID = getIntent().getIntExtra(Intent_ProductID,0);
        Get_WhatsNew();

   listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        startActivity(new Intent(WhatsNew_Activity.this,
                ProductDetails_Activity.class)
                .putExtra(Intent_Category,Whatnew_list.get(position).getProductCategory())
                .putExtra(Intent_SubCAtegory,Whatnew_list.get(position).getProductSubCategory())
                .putExtra(Intent_ProductName,Whatnew_list.get(position).getProductName())
                .putExtra(Intent_ProductID,Whatnew_list.get(position).getId())
        );

     }
});
        

    }

    private void Get_WhatsNew() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading data... please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<WhatsNewResult> call = service.WHATS_NEW_RESULT_CALL();
            call.enqueue(new Callback<WhatsNewResult>() {
                @Override
                public void onResponse(Call<WhatsNewResult> call, Response<WhatsNewResult> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {

                        case "Success": {
                            progressDialog.dismiss();
                            Whatnew_list = response.body().getData();
                            listView.setAdapter(new Whatnew_Adapter(WhatsNew_Activity.this, Whatnew_list));
                            break;
                        }
                        case "NotSuccess": {
                            progressDialog.dismiss();
                            alert.showAlertbox("No Products Found");
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        case "error": {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        default: {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<WhatsNewResult> call, Throwable t) {
                    progressDialog.dismiss();
                    progressDialog.dismiss();
                    alert.showAlertbox(getString(R.string.ServerErrorMessage));
                    alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });

                }

            });

        }catch (Exception ex)
        {
            Log.v("Download Exception",ex.getMessage());
            alert.showAlertbox(getString(R.string.ServerErrorMessage));
            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }
    }


    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(WhatsNew_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(WhatsNew_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(WhatsNew_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(WhatsNew_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(WhatsNew_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(WhatsNew_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;

                    case R.id.contact:
                        Intent intent = new Intent(WhatsNew_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(WhatsNew_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.whats).setVisible(false);
        popupMenu.show();

    }

}
