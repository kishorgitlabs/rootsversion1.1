package com.roots.brainmagic.rootsindia;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFViewer_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PDFView pdfView = (PDFView) findViewById(R.id.pdfView);


        pdfView.fromUri(Uri.parse("http://roots.brainmagicllc.com/ImageUpload/Pads.pdf"));
    }

}
