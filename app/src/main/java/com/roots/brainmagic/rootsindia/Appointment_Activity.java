package com.roots.brainmagic.rootsindia;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;

import customedittext.CustomEditText;
import customedittext.DrawableClickListener;
import model.appointment.AppointmentResult;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared.Shared;
import toaster.Toasts;

import static shared.Shared.K_RegisterEmail;
import static shared.Shared.K_RegisterMobile;
import static shared.Shared.K_RegisterName;

public class Appointment_Activity extends AppCompatActivity {
    @BindView(R.id.name)
    EditText Name;
    @BindView(R.id.mobile)
    EditText Mobile;
    @BindView(R.id.email)
    EditText Email;
    @BindView(R.id.address)
    EditText Address;
    @BindView(R.id.product)
    EditText Product;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.companname)
    EditText companname;
    @BindView(R.id.app_date)
    CustomEditText App_Date;
    @BindView(R.id.app_time)
    CustomEditText App_Time;
    @BindView(R.id.description)
    EditText Description;
    NetworkConnection network = new NetworkConnection(Appointment_Activity.this);
    Alertbox alert = new Alertbox(Appointment_Activity.this);
    Toasts toasts = new Toasts(Appointment_Activity.this);
    String S_Name, S_Mobile, S_Email, S_Address, S_Product, S_App_Date, S_Description,S_App_Time,S_App_City,S_App_company;
    SharedPreferences myshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        ButterKnife.bind(this);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

        Name.setText(myshare.getString(K_RegisterName,""));
        Mobile.setText(myshare.getString(K_RegisterMobile,""));
        Email.setText(myshare.getString(K_RegisterEmail,""));
        App_Date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {
                        App_Date.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_cancel2, 0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }


            }
        });
        App_Date.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        App_Date.getText().clear();
                        App_Date.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        break;
                    default:
                        break;
                }
            }

        });
        App_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                final Calendar c2 = Calendar.getInstance();
                final DatePickerDialog dpd = new DatePickerDialog(Appointment_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                if(c.getTime().compareTo(c2.getTime()) >= 0) {
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                    App_Date.setText(sdf.format(c.getTime()));
                                    Log.v("ChequeDate", App_Date.getText().toString());
                                }
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                dpd.show();
            }
        });

        App_Time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {
                        App_Time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_cancel2, 0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }


            }
        });
        App_Time.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        App_Time.getText().clear();
                        App_Time.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        break;
                    default:
                        break;
                }
            }

        });
        App_Time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Appointment_Activity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mcurrentTime.set(1, 1,2018,selectedHour,selectedMinute);
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.US);
                        App_Time.setText(sdf.format(mcurrentTime.getTime()));


//                        //("hh:mm aa")
                   //     App_Time.setText(String.format("%d:%d", selectedHour, selectedMinute));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

    }

    public void Update_Details(View view) {
        S_Name = Name.getText().toString();
        S_Mobile = Mobile.getText().toString();
        S_Email = Email.getText().toString();
        S_Address = Address.getText().toString();
        S_Product = Product.getText().toString();
        S_App_Date = App_Date.getText().toString();
        S_Description = Description.getText().toString();
        S_App_Time = App_Time.getText().toString();
        S_App_City=city.getText().toString();
        S_App_company=companname.getText().toString();


        if (S_Name.length() == 0) {
            toasts.ShowErrorToast("Please enter Name");
        } else if (S_Mobile.length() == 0) {
            toasts.ShowErrorToast("Please enter Mobile number");
        } else if (S_Mobile.length() < 10) {
            toasts.ShowErrorToast("Enter valid Mobile number");
        } else if (S_Product.length() == 0) {
            toasts.ShowErrorToast("Please enter Product name");
        } else if (S_App_Date.length() == 0) {
            toasts.ShowErrorToast("Please select Date");
        } else if (S_App_Time.length() == 0) {
            toasts.ShowErrorToast("Please select Time");
        }
        else if (S_App_City.length() == 0) {
            toasts.ShowErrorToast("Please select City");
        }
        else if (S_App_company.length() == 0) {
            toasts.ShowErrorToast("Please select Company");
        } else if (S_Email.length() > 0) {

            if(!isValidEmail(S_Email))
                toasts.ShowErrorToast("Enter Valid Email");
            else if (network.CheckInternet()) {
                Update_Appointment();
            } else {
                alert.showAlertbox(getString(R.string.NoNetowrk));
            }
        }


        //isValidEmail
        else if (network.CheckInternet()) {
            Update_Appointment();
        } else {
            alert.showAlertbox(getString(R.string.NoNetowrk));
        }

    }

    private void Update_Appointment() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Updating please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<AppointmentResult> call = service.APPOINTMENT_RESULT_CALL(
                    S_Name,
                    S_Mobile,
                    S_Email,
                    S_Address,
                    S_Product,
                    S_App_Date,
                    S_App_Time,
                    S_Description,
                    S_App_City,
                    S_App_company
            );
            call.enqueue(new Callback<AppointmentResult>() {
                @Override
                public void onResponse(Call<AppointmentResult> call, Response<AppointmentResult> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {

                        case "Success": {

                            progressDialog.dismiss();
                            alert.showAlertbox("Thank you for contacting Roots!\nWe will contact you at the earliest");
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            break;
                        }
                        case "error": {
                            progressDialog.dismiss();
                            alert.showAlertbox("Error in booking Appointment");

                            break;
                        }
                        default: {
                            progressDialog.dismiss();

                            alert.showAlertbox("Server error in booking Appointment");
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<AppointmentResult> call, Throwable t) {
                    progressDialog.dismiss();
                   alert.showAlertbox("Server error in booking Appointment");
                }
            });

        } catch (Exception ex) {
            Log.v("Download Exception", ex.getMessage());
            alert.showAlertbox("Server error in booking Appointment");
        }
    }
    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(Appointment_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(Appointment_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(Appointment_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(Appointment_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(Appointment_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(Appointment_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(Appointment_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(Appointment_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(Appointment_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.appointment).setVisible(false);
        popupMenu.show();

    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


}
