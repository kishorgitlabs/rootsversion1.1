package com.roots.brainmagic.rootsindia;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import adapter.Videos_Adapter;
import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.videos.Datum;
import model.videos.VideosResult;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static indent.Intent_Constants.Intent_VideoCategory;
import static indent.Intent_Constants.Intent_VideoID;

public class Videos_Activity extends AppCompatActivity {
    @BindView(R.id.categorieslistview)
    ListView Videos_listView;
    private List<Datum> Videos_list;
    Alertbox alert = new Alertbox(this);
    NetworkConnection network = new NetworkConnection(Videos_Activity.this);
    String VideoCategory;
    final static String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    final Pattern p = Pattern.compile("^\\d+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        ButterKnife.bind(this);


        //context.startActivity(new Intent(context, Videos_Activity.class).putExtra(Intent_VideoCategory,Datumists.get(position).getVideoCatagoryname()));
        VideoCategory = getIntent().getStringExtra(Intent_VideoCategory);

        Videos_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String videoId = getVideoId(Videos_list.get(position).getVideoURL());
                if (videoId.equals("")) {
                    alert.showAlertbox("Error in loading video");
                } else if (network.CheckInternet()) {
         /*   Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(product.getPdfURL()));
            startActivity(browserIntent);
*/
         startActivity(new Intent(Videos_Activity.this,Video_player_Activity.class).putExtra(Intent_VideoID,videoId)
         );
                } else
                    alert.showAlertbox(getString(R.string.NoNetowrk));

/*
                    AlertDialog alertDialog = new AlertDialog.Builder(

                            Videos_Activity.this).create();

                    LayoutInflater inflater = ((Videos_Activity.this)).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.video_view, null);
                    alertDialog.setView(dialogView);
                    final YouTubePlayerView youTubePlayerView;
                    youTubePlayerView = (YouTubePlayerView) dialogView.findViewById(R.id.youtube_player_view);

                    youTubePlayerView.initialize(new YouTubePlayerInitListener() {
                        @Override
                        public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                            initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                @Override
                                public void onReady() {
                                    // String videoId = "6JYIGclVQdw";
                                    initializedYouTubePlayer.loadVideo(videoId, 0);
                                }

                            });
                        }
                    }, true);

                    youTubePlayerView.toggleFullScreen();

                   *//* youTubePlayerView.initialize(new AbstractYouTubeListener() {
                        @Override
                        public void onReady() {
                            youTubePlayerView.loadVideo(getIntent().getStringExtra("URL"), 0);
                        }
                    }, true);

                    youTubePlayerView.toggleFullScreen();*//*

                    alertDialog.show();*/




            }
        });
        if (network.CheckInternet())
            Get_Videos(VideoCategory);
        else
            alert.showAlertbox(getString(R.string.NoNetowrk));


    }

    private void Get_Videos(String VideoCategory) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading data... please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<VideosResult> call = service.VIDEOS_RESULT_CALL(VideoCategory);
            call.enqueue(new Callback<VideosResult>() {
                @Override
                public void onResponse(Call<VideosResult> call, Response<VideosResult> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {

                        case "Success": {
                            progressDialog.dismiss();
                            Videos_list = response.body().getData();
                            Videos_listView.setAdapter(new Videos_Adapter(Videos_Activity.this, Videos_list));
                            break;
                        }
                        case "NotSuccess": {
                            progressDialog.dismiss();
                            alert.showAlertbox("No data found!");
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        case "error": {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        default: {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<VideosResult> call, Throwable t) {
                    progressDialog.dismiss();
                    progressDialog.dismiss();
                    alert.showAlertbox(getString(R.string.ServerErrorMessage));
                    alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });

                }
            });

        } catch (Exception ex) {
            Log.v("Download Exception", ex.getMessage());
            alert.showAlertbox(getString(R.string.ServerErrorMessage));
            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }
    }

    //Get Video ID
    public static String getVideoId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return "";

        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return "";
    }

    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(Videos_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(Videos_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(Videos_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(Videos_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(Videos_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(Videos_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(Videos_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(Videos_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(Videos_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.videos).setVisible(false);
        popupMenu.show();

    }

}





//WebView webView = (WebView) dialogView.findViewById(R.id.youtube_player_view);

                 /*   int screenWidth = view.getResources().getDisplayMetrics().widthPixels;

                    String frameVideo = "<html><body>Video From YouTube<br><iframe width=\"1040\" height=\"840\" src=\""+Videos_list.get(position).getVideoURL()+"\" frameborder=\"0\" allowfullscreen></iframe></body></html>";


                    webView.setLayoutParams(new RelativeLayout.LayoutParams(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                    webView.getSettings().setLoadWithOverviewMode(true);
                    webView.getSettings().setUseWideViewPort(true);
                    webView.loadData(frameVideo, "text/html", "utf-8");
*/

                    /*  webView.getSettings().setJavaScriptEnabled(true);
                    //http://docs.google.com/gview?embedded=true&url=
                    webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+product.getPdfURL());
                    //webView.loadUrl(product.getPdfURL());
                    //webView.loadUrl("http://roots.brainmagicllc.com/ImageUpload/PB-06-2018.pdf");
                    */