package com.roots.brainmagic.rootsindia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import java.io.File;
import java.io.IOException;
import java.util.List;

import adapter.DocAdapter;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.UploadDoc.Doc;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatalogueActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    LinearLayout Headerlayout;
    RecyclerView recyclerView;
    DocAdapter docAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);

        ButterKnife.bind(this);
        LinearLayout Menulayout = Headerlayout.findViewById(R.id.menu);
//        Menulayout.setVisibility(View.GONE);


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Updating please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
        Call<Doc> call = service.GetDoc();
        call.enqueue(new Callback<Doc>() {
            @Override
            public void onResponse(Call<Doc> call, Response<Doc> response) {
                try {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();


                        recyclerView = findViewById(R.id.rv);
                        docAdapter = new DocAdapter(response.body().getData(), CatalogueActivity.this);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CatalogueActivity.this);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(docAdapter);

                    } else if (response.body().getResult().equals("Not Success")) {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Doc> call, Throwable t) {

            }
        });
    }

    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(CatalogueActivity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(CatalogueActivity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(CatalogueActivity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(CatalogueActivity.this, WhatsNew_Activity.class);
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(CatalogueActivity.this, Search_Products_Activity.class);
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(CatalogueActivity.this, VideoCategory_Activity.class);
                        startActivity(enquiry);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(CatalogueActivity.this, Contact_Activity.class);
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(CatalogueActivity.this, Appointment_Activity.class);
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
        popupMenu.getMenu().findItem(R.id.home).setVisible(false);
        popupMenu.show();

    }
}
