package com.roots.brainmagic.rootsindia;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;

import model.register.RegisterResult;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared.Shared;
import toaster.Toasts;

import static shared.Shared.K_Register;
import static shared.Shared.K_RegisterEmail;
import static shared.Shared.K_RegisterMobile;
import static shared.Shared.K_RegisterName;

public class Register_Activity extends AppCompatActivity {
    @BindView(R.id.name)
    EditText Name;
    @BindView(R.id.mobile)
    EditText Mobile;
    @BindView(R.id.email)
    EditText Email;
    @BindView(R.id.city)
    EditText City;
    @BindView(R.id.state_pinner)
    Spinner StateSpinner;

    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(Register_Activity.this);
    Alertbox alert = new Alertbox(Register_Activity.this);
    Toasts toasts = new Toasts(Register_Activity.this);
    private String State;
    private LinearLayout mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.state_arrays, R.layout.spinner_item);
        StateSpinner.setAdapter(adapter);


    }

    private void Update_Registration() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Registering please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<RegisterResult> call = service.REGISTER_RESULT_CALL(
                    Name.getText().toString(),
                    Email.getText().toString(),
                    Mobile.getText().toString(),
                    City.getText().toString(),
                    State,
                    "",
                    "Android"

            );
            call.enqueue(new Callback<RegisterResult>() {
                @Override
                public void onResponse(Call<RegisterResult> call, Response<RegisterResult> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {

                        case "Success": {
                            editor.putBoolean(K_Register, true);
                            editor.putString(K_RegisterName, Name.getText().toString());
                            editor.putString(K_RegisterMobile, Mobile.getText().toString());
                            editor.putString(K_RegisterEmail, Email.getText().toString());
                            editor.apply();
                            alert.showAlertbox("Registration Successful");
                            progressDialog.dismiss();
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                    startActivity(new Intent(Register_Activity.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));


                                }
                            });
                            //alert.alertDialog.setOnDismissListener( new );

                            break;
                        }
                        case "error": {
                            progressDialog.dismiss();
                            editor.putBoolean(K_Register, false);
                            editor.apply();
                            alert.showAlertbox("Error in Registration");

                            break;
                        }
                        default: {
                            progressDialog.dismiss();
                            editor.putBoolean(K_Register, false);
                            editor.apply();
                            alert.showAlertbox("Server error in Registration");
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<RegisterResult> call, Throwable t) {
                    progressDialog.dismiss();
                    editor.putBoolean(K_Register, false);
                    editor.apply();
                    alert.showAlertbox("Server error in Registration");


                }
            });

        } catch (Exception ex) {
            Log.v("Download Exception", ex.getMessage());
            editor.putBoolean(K_Register, false);
            editor.apply();
            alert.showAlertbox("Server error in Registration");
        }
    }


    public void Register(View view) {
        State = StateSpinner.getSelectedItem().toString();
        if (Name.getText().toString().length() == 0) {
            toasts.ShowErrorToast("Please enter Name");
        } else if (Mobile.getText().toString().length() == 0) {
            toasts.ShowErrorToast("Please enter Mobile number");
        } else if (Mobile.getText().toString().length() < 10) {
            toasts.ShowErrorToast("Enter valid Mobile number");
        } else if (State.equals("Select State")) {
            toasts.ShowErrorToast("Please select State");
        } else if (network.CheckInternet()) {
            Update_Registration();
        } else {
            alert.showAlertbox(getString(R.string.NoNetowrk));
        }
    }
}
