package com.roots.brainmagic.rootsindia;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alert.Alertbox;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.productscontacts.Product;
import network.NetworkConnection;
import presistance.sqlitedatabase.AppDatabase;
import toaster.Toasts;
import uk.co.senab.photoview.PhotoView;

import static indent.Intent_Constants.Intent_Category;
import static indent.Intent_Constants.Intent_ProductID;
import static indent.Intent_Constants.Intent_ProductName;
import static indent.Intent_Constants.Intent_SubCAtegory;
import static indent.Intent_Constants.Intent_VideoID;
import static presistance.sqlitedatabase.AppDatabase.destroyInstance;
import static presistance.sqlitedatabase.AppDatabase.getAppDatabase;
import static presistance.transaction.Transaction.getProductdetails;

public class productwhatsnewActivity extends AppCompatActivity {
    LinearLayout Headerlayout;
    @BindView(R.id.navi)
    TextView navi;
    @BindView(R.id.productname)
    TextView Productname;
    /*@BindView(R.id.description)
    TextView Description;*/
    @BindView(R.id.product_image)
    ImageView ProductImage;
   /* @BindView(R.id.webview)
    WebView webView;*/
    @BindView(R.id.webview_description)
    WebView webview_description;
    @BindView(R.id.pdf_view)
    TextView pdf_view;
  /*  @BindView(R.id.video_view)
    TextView video_view;*/
    @BindView(R.id.desc_text)
    TextView desc_text;
  /*  @BindView(R.id.web_layout)
    LinearLayout web_layout;*/
    Long ProductID;
    AppDatabase db;
    NetworkConnection network = new NetworkConnection(productwhatsnewActivity.this);
    Alertbox alert = new Alertbox(productwhatsnewActivity.this);
    Toasts toasts = new Toasts(productwhatsnewActivity.this);
    Product product;
    final static String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    final Pattern p = Pattern.compile("^\\d+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productwhatsnew);
        ButterKnife.bind(this);
        // LinearLayout Menulayout = Headerlayout.findViewById(R.id.menu);
        //Menulayout.setVisibility(View.INVISIBLE);
        db = getAppDatabase(productwhatsnewActivity.this);
        ProductID = getIntent().getLongExtra(Intent_ProductID, 0);

        if (getIntent().getStringExtra(Intent_Category).equals("")) {
            navi.setText(String.format("%s", getIntent().getStringExtra(Intent_ProductName)));

            navi.setTypeface(navi.getTypeface(), Typeface.BOLD);
            navi.setGravity(Gravity.CENTER);
            Productname.setVisibility(View.GONE);
            /*navi.setTextColor(getResources().getColor(R.color.search_title_text_color));*/
        } else
            navi.setText(String.format("%s >> %s >> %s", getIntent().getStringExtra(Intent_Category), getIntent().getStringExtra(Intent_SubCAtegory), getIntent().getStringExtra(Intent_ProductName)));
        navi.setSelected(true);
        navi.setVisibility(View.GONE);
        Get_Produc_Details();


     /*   Productname.setText(getIntent().getStringExtra(Intent_ProductName));
        try {
            // get input stream
            InputStream ims = getAssets().open(getIntent().getStringExtra(Intent_ProductName)+".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            ProductImage.setImageDrawable(d);
        }
        catch(IOException ignored) {

        }

    */
    }

    private void Get_Produc_Details() {
        product = getProductdetails(db, ProductID);

        if (product != null) {
            //Productname.setText(getIntent().getStringExtra(Intent_ProductName));
            if (product.getPDFName().equals(""))
                pdf_view.setVisibility(View.GONE);
           /* if (product.getVideoName().equals(""))
                video_view.setVisibility(View.GONE);
            if (product.getTechnicalDetails().equals(""))
                web_layout.setVisibility(View.GONE);
*/

            Productname.setText(product.getProductName());
            if (product.getProductDescription().isEmpty())
            {
                desc_text.setVisibility(View.GONE);
                webview_description.setVisibility(View.GONE);
            }
            //webview_description
            // Description.setText(product.getProductDescription());
            try {
                Picasso.with(productwhatsnewActivity.this)
                        .load(product.getProductURL()).error(R.drawable.no_image)
                        .into(ProductImage);
                Log.v("Image URL", product.getProductURL());
            } catch (Exception e) {
                Log.v("Error_Prod_Image", e.getMessage());
                //Picasso.with(context).load(acctistList.get(position).getProductURL()).fit().error(R.drawable.noimage).into(image);
            }
         //   final WebSettings webSettings = webView.getSettings();

            /*Resources res = getResources();
            float fontSize = res.getDimension(R.dimen.txtSize);
            webSettings.setDefaultFontSize((int)fontSize);*/


/*

            webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            webSettings.setAppCacheEnabled(false);
            webSettings.setBlockNetworkImage(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setGeolocationEnabled(false);
            webSettings.setNeedInitialFocus(false);
            webSettings.setSaveFormData(false);
*/


          /*  webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDefaultFontSize(17);
            webView.getSettings().setUseWideViewPort(true);*/
           // webView.loadData(product.getTechnicalDetails(), "text/html", null);
            final WebSettings webSettings2 = webview_description.getSettings();

            /*Resources res = getResources();
            float fontSize = res.getDimension(R.dimen.txtSize);
            webSettings.setDefaultFontSize((int)fontSize);*/



          /*  webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            webSettings.setAppCacheEnabled(false);
            webSettings.setBlockNetworkImage(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setGeolocationEnabled(false);
            webSettings.setNeedInitialFocus(false);
            webSettings.setSaveFormData(false);
*/

          /*  webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDefaultFontSize(17);
            webView.getSettings().setUseWideViewPort(true);*/
            webview_description.loadData(product.getProductDescription(), "text/html", null);
        }
    }

    @Override
    protected void onDestroy() {
        destroyInstance();
        super.onDestroy();
    }



    public void Brochure(View view) {
        if (network.CheckInternet()) {
            // startActivity(new Intent(pro.this,PDFViewer_Activity.class));
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(product.getPdfURL()));
            startActivity(browserIntent);

     /*       //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(product.getPdfURL().replaceAll(" ","%20"))));
            final AlertDialog alertDialog = new AlertDialog.Builder(

                    pro.this).create();

            LayoutInflater inflater = ((pro.this)).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.pdfview, null);
            alertDialog.setView(dialogView);
            PDFView pdfView =(PDFView)   dialogView.findViewById(R.id.pdfView);
            pdfView.fromUri(Uri.parse(product.getPdfURL().replaceAll(" ","%20")));
            final Activity activity = this;
            final TextView title = (TextView) dialogView.findViewById(R.id.title);
            title.setText(product.getProductName());
            WebView webView = (WebView) dialogView.findViewById(R.id.pdfweb);
            webView.getSettings().setJavaScriptEnabled(true);

            webView.setWebChromeClient(new WebChromeClient() {
                                           public void onProgressChanged(WebView view, int progress) {
                                               activity.setTitle("Loading...");
                                               activity.setProgress(progress * 100);
                                               if (progress == 100)
                                                   activity.setTitle("Roots Multiclean");
                                           }
                                       }
            );
            webView.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                }
            });

            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + product.getPdfURL());
            //setContentView(mWebview );
            //webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+product.getPdfURL());

            alertDialog.show();*/
        } else
            alert.showAlertbox(getString(R.string.NoNetowrk));
    }

    public void Video(View view) {

        String videoId = getVideoId(product.getVideoName());
        if (videoId.equals("")) {
            alert.showAlertbox("Error in loading video");
        } else if (network.CheckInternet()) {
            startActivity(new Intent(productwhatsnewActivity.this, Video_player_Activity.class).putExtra(Intent_VideoID, videoId)
            );
        } else
            alert.showAlertbox(getString(R.string.NoNetowrk));
    }


    //Get Video ID
    public static String getVideoId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return "";

        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return "";
    }

    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(productwhatsnewActivity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(productwhatsnewActivity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(productwhatsnewActivity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(productwhatsnewActivity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(productwhatsnewActivity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(productwhatsnewActivity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(productwhatsnewActivity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(productwhatsnewActivity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(productwhatsnewActivity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.products).setVisible(false);
        popupMenu.show();

    }

    public void Image_View(View view) {




        View customView;

        final PopupWindow mPopupWindow;
        PhotoView photoView;
        // Initialize a news instance of LayoutInflater service
        LayoutInflater inflater = (productwhatsnewActivity.this).getLayoutInflater();

        // Inflate the custom layout/view
        customView = inflater.inflate(R.layout.pop_photo, null);

        // Initialize a news instance of popup window
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setFocusable(true);
        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21)
        {
            mPopupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);

        // Set a click listener for the popup window close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            }
        });

        mPopupWindow.showAtLocation(this.findViewById(android.R.id.content).getRootView(), Gravity.CENTER, 0, 0);
        photoView = (PhotoView) customView.findViewById(R.id.pop_photo);


        try {
            Picasso.with(productwhatsnewActivity.this)
                    .load(product.getProductURL()). error(R.drawable.noimage).into(photoView);

            //Picasso.with(context).load(acctistList.get(position).getProductURL()).fit().error(R.drawable.noimage).into(photoView);

        } catch (Exception e) {

            e.printStackTrace();
        }


    }
    }

