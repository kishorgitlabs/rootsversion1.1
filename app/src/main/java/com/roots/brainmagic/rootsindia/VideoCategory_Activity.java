package com.roots.brainmagic.rootsindia;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.util.List;

import adapter.VideoCategory_Adapter;
import alert.Alertbox;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;

import model.videocategory.Datum;
import model.videocategory.VideoCategory;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoCategory_Activity extends AppCompatActivity {
    @BindView(R.id.categorieslistview)
    ListView cat_listView;
    private List<Datum> Whatnew_list;
    Alertbox alert = new Alertbox(this);
    NetworkConnection network = new NetworkConnection(this);

    /*@BindView(R.id.webview)
    WebView webView;
    Handler handler;
   */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_category);
        ButterKnife.bind(this);


      /*  String frameVideo = "<html><body>Video From YouTube<br><iframe width=\"420\" height=\"315\" src=\"https://www.youtube.com/watch?v=CCqAdiFBEKA\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

        WebView displayYoutubeVideo = (WebView) findViewById(R.id.webview);
        displayYoutubeVideo.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = displayYoutubeVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        displayYoutubeVideo.loadData(frameVideo, "text/html", "utf-8");

*/

   /*     VideoCategory_Activity.this.runOnUiThread(new Runnable() {
            public void run() {


  //String frameVideo = "<html><body>Youtube video .. <br> " + "<iframe width=\"320\" height=\"315\" src=\"https://www.youtube.com/watch?v=CCqAdiFBEKA\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    final WebView myWebView = (WebView) findViewById(R.id.webview);

                webView.setWebViewClient(new WebViewClient());
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebChromeClient(new WebChromeClient());

                try {
                    Log.e("Status", "tried url");
                  // webView.loadData(frameVideo, "text/html", "utf-8");
                    webView.loadUrl("https://www.youtube.com/watch?v=CCqAdiFBEKA");
                    webView.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("Status", "In Run():");
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Status", "exception");
                }
            }
        });
*/if(network.CheckInternet())
            Get_VideoCategory();
        else
            alert.showAlertbox(getString(R.string.NoNetowrk));



    }

    private void Get_VideoCategory() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading data... please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<VideoCategory> call = service.VIDEO_CATEGORY_CALL();
            call.enqueue(new Callback<VideoCategory>() {
                @Override
                public void onResponse(Call<VideoCategory> call, Response<VideoCategory> response) {
                    // progressDialog.dismiss();
                    switch (response.body().getResult()) {

                        case "Success": {
                            progressDialog.dismiss();
                            Whatnew_list = response.body().getData();
                            cat_listView.setAdapter(new VideoCategory_Adapter(VideoCategory_Activity.this, Whatnew_list));
                            break;
                        }
                        case "NotSuccess": {
                            progressDialog.dismiss();
                            alert.showAlertbox("No data found!");
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        case "error": {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        default: {
                            progressDialog.dismiss();
                            alert.showAlertbox(getString(R.string.ServerErrorMessage));
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<VideoCategory> call, Throwable t) {
                    progressDialog.dismiss();
                    progressDialog.dismiss();
                    alert.showAlertbox(getString(R.string.ServerErrorMessage));
                    alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });

                }
            });

        } catch (Exception ex) {
            Log.v("Download Exception", ex.getMessage());
            alert.showAlertbox(getString(R.string.ServerErrorMessage));
            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }

    }


    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(VideoCategory_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(VideoCategory_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(VideoCategory_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(VideoCategory_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(VideoCategory_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(VideoCategory_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(VideoCategory_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(VideoCategory_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(VideoCategory_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.videos).setVisible(false);
        popupMenu.show();

    }

}