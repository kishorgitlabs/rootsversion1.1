package com.roots.brainmagic.rootsindia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import alert.Alertbox;
import network.NetworkConnection;

import static indent.Intent_Constants.Intent_VideoID;

public class Video_player_Activity extends AppCompatActivity {
    String videoId;
    Alertbox alert = new Alertbox(this);
    NetworkConnection network = new NetworkConnection(Video_player_Activity.this);
    YouTubePlayerView youTubePlayerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);


        videoId = getIntent().getStringExtra(Intent_VideoID);



        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        if (network.CheckInternet()) {


        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        // String videoId = "6JYIGclVQdw";
                        initializedYouTubePlayer.loadVideo(videoId, 0);
                    }

                });
            }
        }, true);
        } else
            alert.showAlertbox(getString(R.string.NoNetowrk));


        youTubePlayerView.toggleFullScreen();



    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        youTubePlayerView.release();
    }
}
