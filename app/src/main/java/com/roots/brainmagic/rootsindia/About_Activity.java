package com.roots.brainmagic.rootsindia;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.rd.PageIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class About_Activity extends AppCompatActivity {
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    private Handler handler;
    private ImageView imageViewDot;
    private int page = 0;
    int delay = 5000; //milliseconds
    private ImagePagerAdapter adapter;

    private int[] mImages = new int[]{R.drawable.welcome1, R.drawable.welcome2, R.drawable.welcome3, R.drawable.welcome4
    };


    Runnable runnable = new Runnable() {
        public void run() {
            if (adapter.getCount() == page) {
                page = 0;
            } else {
                page++;
            }
            viewPager.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        handler = new Handler();

        adapter = new ImagePagerAdapter();
        viewPager.setAdapter(adapter);

        /*CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circlePageIndicator);
        circlePageIndicator.setViewPager(viewPager);*/
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub

            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
                // imageViewDot.setImageResource(mImagesDot[arg0]);
            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });


        pageIndicatorView.setViewPager(viewPager);

    }


    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }


    private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = About_Activity.this;

            ImageView imageView = new ImageView(context);

            int padding = context.getResources().getDimensionPixelSize(
                    R.dimen.padding_medium);
            imageView.setPadding(padding, padding, padding, padding);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            Glide.with(About_Activity.this)
                    .load(mImages[position])
                    .into(imageView);
            //imageView.setImageResource(mImages[position]);

            ((ViewPager) container).addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);

        }

    }


    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(About_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(About_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(About_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(About_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(About_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(About_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(About_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(About_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(About_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
      /*  Menu pop = popupMenu.getMenu();
        if (myshare.getBoolean("islogin",false))
            pop.findItem(R.id.logoutmenu).setVisible(true);
        else
            pop.findItem(R.id.loginmenu).setVisible(true);*/
        popupMenu.getMenu().findItem(R.id.about).setVisible(false);
        popupMenu.show();

    }


}
