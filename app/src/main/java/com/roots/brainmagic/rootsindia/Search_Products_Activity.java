package com.roots.brainmagic.rootsindia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import toaster.Toasts;

import static indent.Intent_Constants.Intent_Search;

public class Search_Products_Activity extends AppCompatActivity {
    @BindView(R.id.search_text)
    EditText SearchText;
    Toasts toasts = new Toasts(Search_Products_Activity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_products);
        ButterKnife.bind(this);

    }
    public void Search_Products(View view) {
        String Search_String = SearchText.getText().toString();
        if(Search_String.length() == 0)
            toasts.ShowErrorToast("Search field cannot be empty");
        else
            startActivity(new Intent(Search_Products_Activity.this,Search_ProductList_Activity.class).putExtra(Intent_Search,Search_String));
    }
    public void Menu(View view) {
        final PopupMenu popupMenu = new PopupMenu(Search_Products_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }

        });


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub

                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(Search_Products_Activity.this, About_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent vehicleseg = new Intent(Search_Products_Activity.this, ProductActivity.class);
                        startActivity(vehicleseg);
                        return true;
                    case R.id.whats:
                        Intent whatsnew = new Intent(Search_Products_Activity.this,WhatsNew_Activity.class );
                        startActivity(whatsnew);
                        return true;
                    case R.id.search:
                        Intent notify = new Intent(Search_Products_Activity.this,Search_Products_Activity.class );
                        startActivity(notify);
                        return true;
                    case R.id.videos:
                        Intent enquiry = new Intent(Search_Products_Activity.this,VideoCategory_Activity.class );
                        startActivity(enquiry);
                        return true;
                    case R.id.home:
                        Intent home = new Intent(Search_Products_Activity.this,Home_Activity.class );
                        startActivity(home);
                        return true;
                    case R.id.contact:
                        Intent intent = new Intent(Search_Products_Activity.this,Contact_Activity.class );
                        startActivity(intent);
                        return true;
                    case R.id.appointment:
                        Intent appointment = new Intent(Search_Products_Activity.this,Appointment_Activity.class );
                        startActivity(appointment);
                        return true;
                }
                return false;
            }

        });

        popupMenu.inflate(R.menu.pop_menu);
        popupMenu.getMenu().findItem(R.id.search).setVisible(false);
        popupMenu.show();

    }
}
