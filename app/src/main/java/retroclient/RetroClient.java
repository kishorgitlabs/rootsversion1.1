package retroclient;


import com.google.gson.GsonBuilder;

import apiservice.APIService;
import presistance.nulltoempty.NullStringToEmptyAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Systems02 on 01-Jun-17.
 */

public class RetroClient {
    /********
     * URLS
     *******/
//  private static final String ROOT_URL = "http://roots.brainmagicllc.com";

    private static final String ROOT_URL = "http://brainmagicllc.com/rootstest/";
    //private static final String ROOT_URL = "http://localhost:55928/";


    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder()
                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                        .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    /*RestAdapter adapter = new RestAdapter.Builder()
            .setEndpoint(ApiUtils.ROOT_URL)
            .build();*/


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }

    public static String getString(){
        return ROOT_URL;
    }
}
