package presistance.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import model.productscontacts.Contact;


/**
 * Created by SYSTEM02 on 12/28/2017.
 */

@Dao
public interface ContactsDAO {

    @Query("DELETE FROM contacts")
    void Delete_Contacts();

    @Insert
    void insertAll(List<Contact> contactsList);

   /* // query ="select distinct Customername,C_Id from Product order by Customername asc";
    @Query("SELECT pid,Customername,C_Id FROM products WHERE type = 'Spares'  group by Customername order by Customername asc")
    List<Productist> getProCustomers();

    //query= "select distinct segments from Product where C_Id = '"+c_id+"' order by segments asc";
    @Query("SELECT pid,segments,S_Id FROM products WHERE C_Id = :C_id AND type = 'Spares'  group by S_Id order by segments asc")
    List<Productist> getProCusSegmnets(String C_id);

//query= "select distinct model from Product where segments = '"+Segment+"' and  C_Id = '"+ Sid +"' order by model asc";
    @Query("SELECT pid,model,V_Id FROM products WHERE S_Id = :Sid AND type = 'Spares'  group by V_Id order by model asc")
    List<Productist> getProCusSegModel(String Sid);

    //query= "select distinct category from Product where C_Id = '"+ V_id +"' and segments =  '"+Segment+"' and model =  '"+Model_name+"' order by category asc";
    @Query("SELECT pid,category,Proca_Id,Product_Id FROM products WHERE V_Id = :Vid AND type = 'Spares'  group by Proca_Id order by category asc")
    List<Productist> getProCusSegCategory(String Vid);

    //query= "select distinct category from Product where C_Id = '"+ V_id +"' and segments =  '"+Segment+"' and model =  '"+Model_name+"' order by category asc";
    @Query("SELECT * FROM products WHERE Proca_Id = :ProdId AND type = 'Spares'")
    List<Productist> getProdetails(String ProdId);
    //

    @Query("SELECT pid,segments,S_Id FROM products WHERE type = 'Spares'  group by segments order by segments asc")
    List<Productist> getSegments();

    @Query("SELECT pid,Customername,C_Id FROM products WHERE segments = :Segment AND type = 'Spares'  group by C_Id order by Customername asc")
    List<Productist> getSegCus(String Segment);
    //getSegCus


    @Query("SELECT pid,model,V_Id FROM products WHERE segments = :Segment AND C_Id = :C_Id AND type = 'Spares'  group by V_Id order by model asc")
    List<Productist> getSegCusModel(String Segment, String C_Id);


    @Query("SELECT pid,category FROM products WHERE type = 'Spares'  group by category order by category asc")
    List<Productist> getCategory();//getCategory

    @Query("SELECT pid,segments,S_Id FROM products WHERE category = :Category AND type = 'Spares'  group by segments order by segments asc")
    List<Productist> getCatSegments(String Category);//getCategory
    //getCatSegments

    @Query("SELECT pid,Customername,C_Id FROM products WHERE segments = :Segment AND category = :Category AND type = 'Spares'  group by Customername order by Customername asc")
    List<Productist> getCatSegOEM(String Segment, String Category);//getCategory
    //getCatSegments

    @Query("SELECT pid,model,V_Id,Proca_Id FROM products WHERE segments = :Segment AND category = :Category  AND C_Id = :Code AND type = 'Spares'  group by V_Id order by model asc")
    List<Productist> getCatSegOEMModel(String Segment, String Category, String Code);//getCategory


    @Query("SELECT * FROM products where Customername LIKE :SearchString OR segments LIKE :SearchString OR model LIKE :SearchString OR category LIKE :SearchString OR description  LIKE :SearchString OR partno  LIKE :SearchString OR oempartno LIKE :SearchString AND Type = 'Spares' order by category asc")
    List<Productist> searchSpares(String SearchString);*/

}
