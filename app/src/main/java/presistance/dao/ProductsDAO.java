package presistance.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import model.productscontacts.Contact;
import model.productscontacts.Product;


/**
 * Created by SYSTEM02 on 12/28/2017.
 */

@Dao
public interface ProductsDAO {

    @Query("DELETE FROM products")
    void Delete_Products();

    @Insert
    void insertAll(List<Product> productList );

    @Query("SELECT distinct ProductCategory FROM products ORDER BY Productposition ASC ")
    List<String>
    getMainCategory();

    @Query("SELECT distinct ProductSubCategory FROM products WHERE ProductCategory = :ProductCategory  order by Productposition asc")
    List<String> getSubCategory(String ProductCategory);

    @Query("SELECT mId,ProductName,ProductURL,Productposition FROM products WHERE ProductCategory = :ProductCategory AND ProductSubCategory = :ProductSubCategory order by cast(Productposition as int) ")
    List<Product> getProductlist(String ProductCategory,String ProductSubCategory);

    @Query("SELECT * FROM products WHERE mId = :ProductID")
    Product getProductdetails(Long ProductID);

    @Query("SELECT * FROM products where ProductName LIKE :SearchString OR ProductCategory LIKE :SearchString OR ProductSubCategory LIKE :SearchString OR ProductDescription LIKE :SearchString order by Productposition asc")
    List<Product> getSearchResult(String SearchString);

}
