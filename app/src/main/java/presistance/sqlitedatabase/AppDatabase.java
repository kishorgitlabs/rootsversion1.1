package presistance.sqlitedatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


import model.productscontacts.Contact;
import model.productscontacts.Product;
import presistance.dao.ContactsDAO;
import presistance.dao.ProductsDAO;


/**
 * Created by SYSTEM02 on 12/28/2017.
 */

@Database(entities = {Product.class, Contact.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract ProductsDAO productsDAO();

    public abstract ContactsDAO contactsDAO();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "roots-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance()     {
        INSTANCE = null;
    }
}