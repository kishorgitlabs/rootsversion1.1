package presistance.transaction;

import java.util.List;

import model.productscontacts.Contact;
import model.productscontacts.Product;
import presistance.sqlitedatabase.AppDatabase;

/**
 * Created by SYSTEM02 on 3/24/2018.
 */

 public class Transaction {
    public static void deletePrducts(final AppDatabase db) {
        db.productsDAO().Delete_Products();
    }
    public static void addProducts(final AppDatabase db, List<Product> products) {
        db.productsDAO().insertAll(products);
    }
    public static void deleteContacts(final AppDatabase db) {
        db.contactsDAO().Delete_Contacts();
    }
    public static void addContacts(final AppDatabase db, List<Contact> contacts) {
        db.contactsDAO().insertAll(contacts);
    }
    //getMainCategory
    public static List<String> getMainCategory(final AppDatabase db) {
        return db.productsDAO().getMainCategory();
    }
    //getProductlist
    public static List<String> getProductlist(final AppDatabase db, String MainCategory) {
        return db.productsDAO().getSubCategory(MainCategory);
    }
    //getProductlist
    public static List<Product> getProductlist(final AppDatabase db, String MainCategory, String SubCategory) {
        return db.productsDAO().getProductlist(MainCategory,SubCategory);
    }
    //getProductDetails
    public static Product getProductdetails(final AppDatabase db, Long ProductID) {
        return db.productsDAO().getProductdetails(ProductID);
    }
    //getSearchResult
    public static List<Product> getSearchResult(final AppDatabase db, String SearchString) {
        return db.productsDAO().getSearchResult(SearchString);
    }

}
