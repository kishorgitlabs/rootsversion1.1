
package model.productscontacts;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Entity(tableName = "contacts")
public class Contact {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private Long mId;
    @ColumnInfo(name = "Address")
    @SerializedName("Address")
    private String mAddress;
    @ColumnInfo(name = "City")
    @SerializedName("City")
    private String mCity;
    @ColumnInfo(name = "EmailId")
    @SerializedName("EmailId")
    private String mEmailId;
    @ColumnInfo(name = "Landline")
    @SerializedName("Landline")
    private String mLandline;
    @ColumnInfo(name = "MobileNo")
    @SerializedName("MobileNo")
    private String mMobileNo;
    @ColumnInfo(name = "Pincode")
    @SerializedName("Pincode")
    private String mPincode;
    @ColumnInfo(name = "State")
    @SerializedName("State")
    private String mState;
    @ColumnInfo(name = "Status")
    @SerializedName("Status")
    private String mStatus;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLandline() {
        return mLandline;
    }

    public void setLandline(String Landline) {
        mLandline = Landline;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String MobileNo) {
        mMobileNo = MobileNo;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

}
