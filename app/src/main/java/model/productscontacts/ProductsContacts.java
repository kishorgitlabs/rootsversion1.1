
package model.productscontacts;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ProductsContacts {

    @SerializedName("contact")
    private List<Contact> mContact;
    @SerializedName("product")
    private List<Product> mProduct;
    @SerializedName("result")
    private String mResult;

    public List<Contact> getContact() {
        return mContact;
    }

    public void setContact(List<Contact> contact) {
        mContact = contact;
    }

    public List<Product> getProduct() {
        return mProduct;
    }

    public void setProduct(List<Product> product) {
        mProduct = product;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
