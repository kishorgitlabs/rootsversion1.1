
package model.productscontacts;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Entity(tableName = "products")
public class Product {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private Long mId;
    @ColumnInfo(name = "Image1")
    @SerializedName("Image1")
    private String mImage1;
    @ColumnInfo(name = "Image2")
    @SerializedName("Image2")
    private String mImage2;
    @ColumnInfo(name = "Image3")
    @SerializedName("Image3")
    private String mImage3;
    @ColumnInfo(name = "Image4")
    @SerializedName("Image4")
    private String mImage4;
    @ColumnInfo(name = "InsertDate")
    @SerializedName("InsertDate")
    private String mInsertDate;
    @ColumnInfo(name = "PDFName")
    @SerializedName("PDFName")
    private String mPDFName;
    @ColumnInfo(name = "PdfURL")
    @SerializedName("PdfURL")
    private String mPdfURL;
    @ColumnInfo(name = "ProductCategory")
    @SerializedName("ProductCategory")
    private String mProductCategory;
    @ColumnInfo(name = "ProductDescription")
    @SerializedName("ProductDescription")
    private String mProductDescription;
    @ColumnInfo(name = "ProductImage")
    @SerializedName("ProductImage")
    private String mProductImage;
    @ColumnInfo(name = "ProductName")
    @SerializedName("ProductName")
    private String mProductName;
    @ColumnInfo(name = "ProductSubCategory")
    @SerializedName("ProductSubCategory")
    private String mProductSubCategory;
    @ColumnInfo(name = "ProductURL")
    @SerializedName("ProductURL")
    private String mProductURL;
    @ColumnInfo(name = "Status")
    @SerializedName("Status")
    private String mStatus;
    @ColumnInfo(name = "UpdateDate")
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @ColumnInfo(name = "VideoName")
    @SerializedName("VideoName")
    private String mVideoName;
    @ColumnInfo(name = "WhatsNew")
    @SerializedName("WhatsNew")
    private String mWhatsNew;

    @ColumnInfo(name = "TechnicalDetails")
    @SerializedName("TechnicalDetails")
    private String mTechnicalDetails;


    @ColumnInfo(name = "Productposition")
    @SerializedName("Productposition")
    private String Productposition;

    @ColumnInfo(name = "position")
    @SerializedName("position")
    private String position;

    public String getProductposition() {
        return Productposition;
    }

    public void setProductposition(String productposition) {
        Productposition = productposition;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String mposition) {
        this.position = mposition;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String Image1) {
        mImage1 = Image1;
    }

    public String getImage2() {
        return mImage2;
    }

    public void setImage2(String Image2) {
        mImage2 = Image2;
    }

    public String getImage3() {
        return mImage3;
    }

    public void setImage3(String Image3) {
        mImage3 = Image3;
    }

    public String getImage4() {
        return mImage4;
    }

    public void setImage4(String Image4) {
        mImage4 = Image4;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String InsertDate) {
        mInsertDate = InsertDate;
    }

    public String getPDFName() {
        return mPDFName;
    }

    public void setPDFName(String PDFName) {
        mPDFName = PDFName;
    }

    public String getPdfURL() {
        return mPdfURL;
    }

    public void setPdfURL(String PdfURL) {
        mPdfURL = PdfURL;
    }

    public String getProductCategory() {
        return mProductCategory;
    }

    public void setProductCategory(String ProductCategory) {
        mProductCategory = ProductCategory;
    }

    public String getProductDescription() {
        return mProductDescription;
    }

    public void setProductDescription(String ProductDescription) {
        mProductDescription = ProductDescription;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String ProductImage) {
        mProductImage = ProductImage;
    }

    public String getProductName() {
        return mProductName;
    }
    public void setProductName(String ProductName) {
        mProductName = ProductName;
    }

    public String getProductSubCategory() {
        return mProductSubCategory;
    }

    public void setProductSubCategory(String ProductSubCategory) {
        mProductSubCategory = ProductSubCategory;
    }

    public String getProductURL() {
        return mProductURL;
    }

    public void setProductURL(String ProductURL) {
        mProductURL = ProductURL;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        mUpdateDate = UpdateDate;
    }

    public String getVideoName() {
        return mVideoName;
    }

    public void setVideoName(String VideoName) {
        mVideoName = VideoName;
    }

    public String getWhatsNew() {
        return mWhatsNew;
    }

    public void setWhatsNew(String WhatsNew) {
        mWhatsNew = WhatsNew;
    }

    public String getTechnicalDetails() {
        return mTechnicalDetails;
    }

    public void setTechnicalDetails(String TechnicalDetails) {
        this.mTechnicalDetails = TechnicalDetails;
    }
}
