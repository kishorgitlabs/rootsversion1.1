
package model.videos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("videoURL")
    private String mVideoURL;
    @SerializedName("Videoname")
    private String mVideoname;

    public String getVideoURL() {
        return mVideoURL;
    }

    public void setVideoURL(String videoURL) {
        mVideoURL = videoURL;
    }

    public String getVideoname() {
        return mVideoname;
    }

    public void setVideoname(String Videoname) {
        mVideoname = Videoname;
    }

}
