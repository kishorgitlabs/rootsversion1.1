
package model.appointment;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("DateTime")
    private String mDateTime;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("EmailId")
    private String mEmailId;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String DateTime) {
        mDateTime = DateTime;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String InsertDate) {
        mInsertDate = InsertDate;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String MobileNo) {
        mMobileNo = MobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String ProductName) {
        mProductName = ProductName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object UpdateDate) {
        mUpdateDate = UpdateDate;
    }

}
