
package model.whatsnew;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("id")
    private Long mId;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Image2")
    private String mImage2;
    @SerializedName("Image3")
    private String mImage3;
    @SerializedName("Image4")
    private String mImage4;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("PDFName")
    private String mPDFName;
    @SerializedName("PdfURL")
    private String mPdfURL;
    @SerializedName("ProductCategory")
    private String mProductCategory;
    @SerializedName("ProductDescription")
    private String mProductDescription;
    @SerializedName("ProductImage")
    private String mProductImage;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("ProductSubCategory")
    private String mProductSubCategory;
    @SerializedName("ProductURL")
    private String mProductURL;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("VideoName")
    private String mVideoName;
    @SerializedName("WhatsNew")
    private String mWhatsNew;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String Image1) {
        mImage1 = Image1;
    }

    public String getImage2() {
        return mImage2;
    }

    public void setImage2(String Image2) {
        mImage2 = Image2;
    }

    public String getImage3() {
        return mImage3;
    }

    public void setImage3(String Image3) {
        mImage3 = Image3;
    }

    public String getImage4() {
        return mImage4;
    }

    public void setImage4(String Image4) {
        mImage4 = Image4;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String InsertDate) {
        mInsertDate = InsertDate;
    }

    public String getPDFName() {
        return mPDFName;
    }

    public void setPDFName(String PDFName) {
        mPDFName = PDFName;
    }

    public String getPdfURL() {
        return mPdfURL;
    }

    public void setPdfURL(String PdfURL) {
        mPdfURL = PdfURL;
    }

    public String getProductCategory() {
        return mProductCategory;
    }

    public void setProductCategory(String ProductCategory) {
        mProductCategory = ProductCategory;
    }

    public String getProductDescription() {
        return mProductDescription;
    }

    public void setProductDescription(String ProductDescription) {
        mProductDescription = ProductDescription;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String ProductImage) {
        mProductImage = ProductImage;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String ProductName) {
        mProductName = ProductName;
    }

    public String getProductSubCategory() {
        return mProductSubCategory;
    }

    public void setProductSubCategory(String ProductSubCategory) {
        mProductSubCategory = ProductSubCategory;
    }

    public String getProductURL() {
        return mProductURL;
    }

    public void setProductURL(String ProductURL) {
        mProductURL = ProductURL;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        mUpdateDate = UpdateDate;
    }

    public String getVideoName() {
        return mVideoName;
    }

    public void setVideoName(String VideoName) {
        mVideoName = VideoName;
    }

    public String getWhatsNew() {
        return mWhatsNew;
    }

    public void setWhatsNew(String WhatsNew) {
        mWhatsNew = WhatsNew;
    }

}
