
package model.videocategory;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Deleteflag")
    private String mDeleteflag;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("VideoCatagory_id")
    private Long mVideoCatagoryId;
    @SerializedName("VideoCatagoryname")
    private String mVideoCatagoryname;

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String Deleteflag) {
        mDeleteflag = Deleteflag;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String InsertDate) {
        mInsertDate = InsertDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        mUpdateDate = UpdateDate;
    }

    public Long getVideoCatagoryId() {
        return mVideoCatagoryId;
    }

    public void setVideoCatagoryId(Long VideoCatagoryId) {
        mVideoCatagoryId = VideoCatagoryId;
    }

    public String getVideoCatagoryname() {
        return mVideoCatagoryname;
    }

    public void setVideoCatagoryname(String VideoCatagoryname) {
        mVideoCatagoryname = VideoCatagoryname;
    }

}
